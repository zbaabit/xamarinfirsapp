﻿using System;
using System.ComponentModel;
using Plugin.Geolocator;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Forms.Maps;

namespace MyFirstApp.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class AboutPage : ContentPage
    {
        public AboutPage()
        {
            InitializeComponent();
            //var tt = new Xamarin.Forms.Maps.Map(MapSpan.FromCenterAndRadius(new Position(36.8961, 10.1865),
            //    Distance.FromMiles(0.5)))
            //{
            //    IsShowingUser = true,
            //    VerticalOptions = LayoutOptions.FillAndExpand
            //};
            map.HeightRequest = 400;
        }

        private async void Button_OnClicked(object sender, EventArgs e)
        {
            try
            {
               
                var location = await Xamarin.Essentials.Geolocation.GetLastKnownLocationAsync();

                if (location != null)
                {
                    Console.WriteLine($"Latitude: {location.Latitude}, Longitude: {location.Longitude}, Altitude: {location.Altitude}");
                }
                var locator = CrossGeolocator.Current;
                var position = await locator.GetPositionAsync(TimeSpan.FromSeconds(10000));
                double tt = double.Parse(position.Speed.ToString()) * 3.6;

                LongitudeLabel.Text = position.Longitude.ToString();
                latitudeLabel.Text = position.Latitude.ToString();
                SpeedLabel.Text = tt.ToString();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                throw;
            }
         
        }
    }
}